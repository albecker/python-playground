import random

user = 0
computer = 0

def computer_number():
    num = random.randrange(1,4)

    if num == 1:
        print("Computer chooses rock")
    elif num == 2:
        print("Computer chooses paper")
    elif num == 3:
        print("Computer chooses scissors")

    return num


def user_guess():

    guess = input("Choose 'rock', 'paper', or 'scissors' by typing that word: ")

    if is_valid_guess(guess):

        if guess == 'rock':
            number = 1

        elif guess == 'paper':
            number = 2

        elif guess == 'scissors':
            number = 3
            
        return number
    else:
        print('That response is invalid.')
        user_guess()

def is_valid_guess(guess):
    if guess in ('rock', 'paper', 'scissors'):
        status = True
    else:
        status = False
    return status

def restart():
    answer = input("Would you like to play again? Enter 'y' for yes or 'n' for no: ")

    if answer == 'y':
        main()
    elif answer == 'n':
        print("Goodbye!")
    else:
        print("Please enter only 'y' or 'n'!")
 
        restart()


def results(num, number):
    difference = num - number
    #if/elif statement
    if difference == 0:
        print("TIE!")
        #call restart

    elif difference % 3 == 1:
        print("I'm sorry! You lost :(")
        global computer
        computer += 1
        #call restart

    elif difference % 3 == 2:
        print("Congratulations! You won :)")
        global user
        user += 1
        #call restart
    
    print("Current Results - You: " + str(user) + " Computer: " + str(computer))    
    restart()
        
def main():
    print("Let's play 'Rock, Paper, Scissors'!")
    global computer
    global user
    
    number = user_guess()

    num = computer_number()

    results(num, number)
   
        
main()