# Todolist class
class Todo:
	items = []
	status = []

	def add(self, item):
		self.items.append(item)
		self.status.append("[ ]")
		
	def remove(self, item):
		try:
			position = self.items.index(item)
			
			if position:
				self.items.remove(item)
		except ValueError:
			print("Item ist not in List")
	
	def clear(self):
		self.items = []
		self.status = []
	
	def check(self, item):
		position = self.items.index(item)
		if (position):
			self.status[position] = "[X]"
	
	def printout(self):
		pos = 0
		for i in self.items:
			print(self.status[pos] + " " + self.items[pos])
			pos += 1