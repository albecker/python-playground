# Print something
print("Hallo Xcode!")

# Type Conversions
for value in [36.6, "32", 32]:
    print(type(value), value, int(value))

def florp():
    print("florpage")

# Tryout function
florp()
